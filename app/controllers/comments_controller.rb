class CommentsController < ApplicationController
  def create
      @comment = Comment.new(comment_params)
      if @comment.save
        flash.now[:success] = " send reply"
        redirect_to micropost_url (params[:micropost_id])
      else
        flash[:danger] = "cann't send"
        redirect_to micropost_url (params[:micropost_id])
      end
  end

  private
  def comment_params
    params.require(:comment).permit(:comment).merge(micropost_id: params[:micropost_id], user_id: current_user.id)
  end
end
